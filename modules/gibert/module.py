from weboob.tools.backend import Module
from weboob.capabilities.base import  Capability, BaseObject
from weboob.capabilities.collection import  CapCollection, Collection, BaseCollection, CollectionNotFound
from weboob.browser import URL, PagesBrowser
from weboob.browser.pages import HTMLPage, pagination, JsonPage
from weboob.browser.elements import method, ListElement, ItemElement
from weboob.browser.filters.html import CleanHTML, Attr, Link
from weboob.capabilities.pricecomparison import Product
from weboob.capabilities.base import StringField
from weboob.browser.filters.standard import CleanText, RawText, CleanDecimal

__all__ = ['GibertModule']



class SearchablePage(HTMLPage):
    def search(self, search_string):
        form = self.get_form()
        form['q'] = search_string
        form.submit()

class BookCategory(BaseObject):
    name = StringField("Name of category")
    url = StringField("url")

class BookProduct(Product):
    name = StringField('Name')
    author = StringField('Author')
    price = StringField('Price')
    thumbnail_url = StringField("thumbnail_url")
    category = StringField("category")
    min_price = StringField("min_price")
    max_price = StringField("max_price")
    details_url = StringField("details_url")
    ean13 = StringField("ean13")
    publisher = StringField("publisher")
    datePublished = StringField("datePublished")
    collection = StringField("collection")
    numberOfPages = StringField("numberOfPages")



class ProductElement(ItemElement):
    klass = BookProduct


class CategoryElement(ItemElement):
    klass = BookCategory


class BookListPage(SearchablePage):
    @pagination
    @method
    class iter_books(ListElement):
        item_xpath = '//li[has-class("product-cell")]'

        next_page = Link('//ul[@class="paginator"]//i[has-class("fa-caret-right")]/parent::a')

        class item(ProductElement):
            obj_id = Attr('.', 'data-productid')
            obj_name= CleanText('.//div[@class="details"]/div/strong')
            obj_author= CleanText('.//div[@class="details"]/div[2]')
            obj_thumbnail_url = Attr('./a/div/img', "src", "")
            obj_details_url = Link('./a')


    @pagination
    @method
    class iter_categories(ListElement):
        item_xpath = '//ul[has-class("search-filter-options")]//li'

        #next_page = Link('//ul[@class="paginator"]//i[has-class("fa-caret-right")]/parent::a')
        ""

        class item(CategoryElement):
            pass
            obj_id= CleanText('./a')
            obj_name= CleanText('./a')
            #obj_url = CleanText('./a')



class CapBookPrice(Capability):
    pass


class WSPage(JsonPage):
    def iter_items(self):
        for key, value in self.doc.items():
            p = BookProduct()
            p.id = key
            p.category = value['category']
            p.min_price = value['min_price']
            p.max_price = value['max_price']

            yield p


class BookDetailPage(SearchablePage):
    @method
    class get_details(ProductElement):
        klass = BookProduct


        obj_name = CleanText('//h1')
        obj_author = CleanText('//h2')
        obj_ean13 = CleanText('//ul[@class="info-table"]/li[1]/strong')
        obj_publisher = CleanText('//ul[@class="info-table"]/li[@itemprop="publisher"]/strong')
        obj_datePublished = Attr('//ul[@class="info-table"]/li[@itemprop="datePublished"]', "datetime")
        obj_collection = CleanText('//ul[@class="info-table"]/li[4]/strong')
        obj_numberOfPages = CleanText('//ul[@class="info-table"]/li[@itemprop="numberOfPages"]/strong')

class AdvancedSearchPage(SearchablePage):
    def advanced_search(
            self,
            code='',
            title="",
            main_contributor_name="",
            publisher_name = "",
            collection_title = ''):

        form = self.get_form('//form[@class="form-horizontal"]')
        form['code'] = code
        form['title'] = title
        form['main_contributor_name'] = main_contributor_name
        form['publisher_name'] = publisher_name
        form['collection_title'] = collection_title
        form.submit()


class CategoryElement(ItemElement):
    klass = Collection



class HomePage(BookListPage):
    pass





class MyBrowser(PagesBrowser):
    BASEURL = 'https://www.gibertjeune.fr/'

    #mypage = URL('boutique/livres/comics/', MyPage)
    #mypage = URL('/boutique/livres/strip/', BookListPage)
    wspage = URL('ws/improve_product_cells/(?P<pattern>.*)', WSPage)
    detailpage = URL('/article/(?P<id>.*)', BookDetailPage)
    simplesearchpage = URL('/recherche/(?P<q>.*)', BookListPage)
    advanced_search_page = URL('https://www.gibertjeune.fr/recherche_avancee/$', AdvancedSearchPage)
    homepage = URL('$', HomePage)
    homepage = URL('/boutique/livres/', HomePage)
    advanced_search_result_page = URL('https://www.gibertjeune.fr/recherche_avancee/', BookListPage)


    def list_items(self):
        return self.mypage.stay_or_go().list_items()

    def iter_items_price(self, id_list):
        pattern = "?product=%s" % ('&product='.join(id_list),)
        return self.wspage.stay_or_go(pattern=pattern).iter_items()

    def get_productinfos(self, id):
        return self.detailpage.stay_or_go(id=id).get_details()

    def simple_search(self, search_string):
        self.homepage.stay_or_go().search(search_string)
        return self.page.iter_books()

    def advanced_search(self, code):
        self.advanced_search_page.stay_or_go().advanced_search(code)
        return self.page.list_items()

    def get_book_info(self, code):
        for i in self.advanced_search(code):
            return i

    def iter_categories(self, url):
        self.logger.debug("iter_categories %s " % (url, ))
        self.homepage.stay_or_go()
        return self.page.iter_categories()

    def get_book(self, _id):
        r = self.detailpage.stay_or_go(id=_id).get_details()
        r.id = _id
        assert r is not None
        return r






class GibertModule(Module, CapBookPrice, CapCollection):
    NAME = "gibert"
    VERSION = '1.2'
    BROWSER = MyBrowser

    def get_book_info(self, _id):
        return self.browser.get_book_info(_id)


    def simple_search(self, pattern):
        for i in self.browser.simple_search(pattern):
            print(i)


    def iter_resources(self, objs, split_path):
        self.logger.debug("iter_resources %s %s " % (objs, split_path))
        collection = self.get_collection(objs, split_path)
        self.logger.debug("collection %s %s" % (collection, collection.path_level))
        self.logger.debug("collection.url %s" % (collection.url,))

        #assert collection.url is not None

        for c in self.browser.iter_categories(collection.url):
            col = Collection([c.name], title=c.name, url=c.url)
            yield col

        #raise NotImplementedError("iter_resources %s, %s" % (objs, split_path))


    def validate_collection(self, objs, collection):
        self.logger.debug("validate_collection %s %s", objs, collection)
        return
        raise CollectionNotFound(collection.split_path)

    def fill_book(self, book, fields):
        if 'min_price' in fields:
            for p in self.browser.iter_items_price((book.id,)):
                book.min_price = p.min_price
                book.max_price = p.max_price
                book.category = p.category
                return book

        raise NotImplementedError("fill_book %s %s " % (book, fields))

    def list_categories(self):
        for c in self.browser.iter_categories():
            print(c)

    def get_book(self, _id):
        book = self.browser.get_book(_id)
        self.fillobj(book, ['min_price',])
        return book

    OBJECTS = {
            BookProduct : fill_book
            }
